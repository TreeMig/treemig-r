remove_simulation <- function(path, data) {
    if (data$deleteSimulationStateFiles) {
        unlink(file.path(path, "S", data$simulationID), recursive = TRUE)
    }
    if (data$deletePlots) {
        unlink(file.path(path, "Plots", data$simulationID), recursive = TRUE)
    }
    if (data$deleteSimulationResults) {
        unlink(file.path(path, "R", data$simulationID), recursive = TRUE)
    }
    unlink(file.path(path, "C", paste0(data$simulationID,".txt")), recursive = TRUE)
    # should it also be possible delete bioclimate and species pars file?
}