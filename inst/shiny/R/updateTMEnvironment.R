loadTMEnvironment <- function(workingDir, session){
  if(isTMEnvironment(workingDir)){
    setwd(workingDir)
    addResourcePath("TMEnvironment",workingDir)

    session$sendCustomMessage("updateBioclimList", listBioclimatesJSON(workingDir))
    session$sendCustomMessage("updateSpeciesParsList", getSpecParsArrayJson(workingDir))
    session$sendCustomMessage("updateStateFileList", listStateFilesJSON(workingDir))

    session$sendCustomMessage("updateSimulationList",  getSimulationsJson(workingDir, "C"))
    session$sendCustomMessage("updateSimulationTemplateList",  getSimulationsJson(workingDir, "C/Templates"))
  }
}