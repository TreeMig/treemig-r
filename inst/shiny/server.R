library(shinyFiles)
library(data.table)
library(shiny)
library(TreeMig)


respond <- function(UID, data, session, errorCode = 0, errorMsg = ""){
  session$sendCustomMessage("fromShiny", list(id=UID, data=data, errorCode=errorCode, errorMsg=errorMsg))
}

sendBioclimateList <- function(session){
  session$sendCustomMessage("updateBioclimList", listBioclimatesJSON(getwd()))
}
sendSpeciesParsList <- function(session){
  session$sendCustomMessage("updateSpeciesParsList", getSpecParsArrayJson(getwd()))
}
sendStateFileList <- function(session){
  session$sendCustomMessage("updateStateFileList", listStateFilesJSON(getwd()))
}

sendConsoleUpdate <- function(type, UID, session, data=""){
  session$sendCustomMessage("console", list(type=type, id=UID, data=data))
}


shouldCancelJob <- function(jobID, cancelList){
  cancelIndex = which(cancelList == jobID)
  if(length(cancelIndex) != 0){
    cancelList = cancelList[-cancelIndex]
    return(TRUE)
  }
  return(FALSE)
}

drives <- getDriveAndDirs()
server <- function(input, output, session) {
  rv <- reactiveValues(currentRun = NULL, simulationQueue = Queue$new(), cancelList = c(integer(0)))
    
  cat("Session started\n")
  shinyFileChoose(input, "sffile", roots = drives, session = session)

  observeEvent(input$sendShiny, {
    data <- input$sendShiny$data
    UID <- input$sendShiny$id
    #print(input$sendShiny)
    switch (input$sendShiny$type,
      runSimulation = {
        queueSimulation(session, data, rv$simulationQueue)
        sendConsoleUpdate("queued", data$jobID, session)
        respond(UID, TRUE, session)
      },
      cancelJob = {
        rv$cancelList[length(rv$cancelList)+1] = data$jobID
        sendConsoleUpdate("cancel_received", data$jobID, session)
        respond(UID, TRUE, session)
      },
      getFileInformation = {
        respond(UID, getFileInformation(data$path), session)
      },
      deleteSimulation = {
        print(data)
        remove_simulation(getwd(), data)
        
        respond(UID, TRUE, session)
      },
      loadTMEnvironment = {
        if(isTMEnvironment(data$path)){
           path <- normalizePath(data$path,winslash = "/")
          path <- gsub("/+",x = path, "/", fixed = FALSE)
          loadTMEnvironment(path, session)
         
          respond(UID, path, session)
        }else{
          if(dir.exists(data$path)){
            respond(UID, NULL, session, 1, "Directory is not a TreeMig folder")
          }else{
            respond(UID, NULL, session, 2, "Directory does not exist!") 
          }
        }
      },
      createTMEnvironment = {
        tryCatch(
          {
            cat(data$path,data$name,"\n")
            createTMEnvironment(data$path,data$name)
            path <- normalizePath(data$path,winslash = "/")
            path_to <- file.path(path, data$name)
            path_to <- gsub("/+",x = path_to, "/", fixed = FALSE)
            loadTMEnvironment(path_to, session);
            respond(UID, path_to, session)
          },
          error=function(cond) {
            print(data)
            respond(UID, NULL, session, 1, "Could not create folder at specified location.")
          }
        )
      },
      fileExists = {
        respond(UID, file.exists(data$path), session)
      },
      getPlots = {
        respond(UID, listPlotsJSON(data$TMDirectory, data$simulationID), session)
      }
	    # later move things here
    )
  })

  observe({
    invalidateLater(700, session)
    if(!is.null(rv$currentRun)){
      isAlive = rv$currentRun$process$is_incomplete_output() && !rv$currentRun$canceled
      if(!rv$currentRun$process$is_alive()){
        rv$currentRun$TTL = rv$currentRun$TTL -1
      }
      try({
        output <- rv$currentRun$process$read_output_lines()
        if(length(output)!=0){
          sendConsoleUpdate("output", rv$currentRun$jobID, session, output)
        }
      }, silent = TRUE)
      if(!isAlive || rv$currentRun$TTL <= 0){
        status = rv$currentRun$process$get_exit_status()
        if(status == 0){
          sendConsoleUpdate("terminated", rv$currentRun$jobID, session)
        }else if(status == 2){
          sendConsoleUpdate("canceled", rv$currentRun$jobID, session)
        }else{
          sendConsoleUpdate("error", rv$currentRun$jobID, session)
        }
        # always update bioclim, species and statefile lists after run
        sendBioclimateList(session)
        sendSpeciesParsList(session)
        sendStateFileList(session)

        rv$currentRun <- NULL;

        # check if simulation was canceled, if so, remove it from the cancel list and pop next one
        repeat{
          nextSim = rv$simulationQueue$pop()
          cancelIndex = which(rv$cancelList == nextSim$jobID)
          if(length(cancelIndex) == 0){
            break
          }          
          rv$cancelList = rv$cancelList[-cancelIndex]
          #session$sendCustomMessage("status", list(type="canceled", id=nextSim$jobID))
          sendConsoleUpdate("canceled", nextSim$jobID, session)
        }
        # queue next simulation
        if(!is.null(nextSim)){
          #session$sendCustomMessage("status", list(type="running", id=nextSim$jobID))
          sendConsoleUpdate("running", nextSim$jobID, session)
          
          nextSim$process  = runCode(nextSim$code)
          rv$currentRun <- nextSim;
        }else{
          # no simulation running and queue is empty, clear cancel list
          rv$cancelList = c(integer(0));
        }        
      }else{
        print("Process is running....")
        # process is running, check if it has been canceled
        cancelIndex = which(rv$cancelList == rv$currentRun$jobID)
        
        print(paste0("cancelIndex: ",cancelIndex," list: ", rv$cancelList, " running: ", rv$currentRun$jobID))
        if(length(cancelIndex) != 0){
          print("TRIYNG TO KILL THIS JOB!")
          try(rv$currentRun$process$kill(), silent = TRUE)
          rv$currentRun$canceled = TRUE;
          rv$cancelList = rv$cancelList[-cancelIndex]   
        }
      }
    }else{
      # NO JOB IS RUNNING
      repeat{          
        nextSim = rv$simulationQueue$pop();       
        cancelIndex = which(rv$cancelList == nextSim$jobID)
        if(length(cancelIndex) == 0){
          break
        }          
        rv$cancelList = rv$cancelList[-cancelIndex]
        #session$sendCustomMessage("status", list(type="canceled", id=nextSim$jobID))
        sendConsoleUpdate("canceled", nextSim$jobID, session)
      }
      # queue next simulation      
      if(!is.null(nextSim)){
        #session$sendCustomMessage("status", list(type="running", id=nextSim$jobID))
        sendConsoleUpdate("running", nextSim$jobID, session)
        nextSim$process  = runCode(nextSim$code)
        rv$currentRun <- nextSim;
      }else{
        # no simulation running and queue is empty, clear cancel list
        rv$cancelList = c(integer(0));
      }
    }
  })
  onStop(function() {
    cat("Session stopped\n")
    suppressWarnings(removeResourcePath("TMEnvironment"))
  })
}


