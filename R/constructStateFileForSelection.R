#'@importFrom utils count.fields
#'@importFrom data.table fwrite fread
constructStateFileForSelection <- function(stateFile, specSel, specAll, specParsFileAll, TMDirectory) {
  if(length(specAll) == 1 && tolower(specAll) == "standard"){
    allSpecies <- fread(file.path(TMDirectory, "P", specParsFileAll), header = TRUE, nrows = 30)$Name
    specAll <- allSpecies
  }

  if(length(specSel) == 1 && tolower(specSel) == "standard"){
    specSel <- specAll
  }

  # for assigning the right rows of the initialvalue file to the selected species and giving them ordering numbers
  specInd <- match(specSel, specAll)
  numbercols <- max(count.fields(file.path(TMDirectory, stateFile)))  # Trick to read all columns
  sf <- fread(file.path(TMDirectory, stateFile), fill = TRUE, header = FALSE, col.names = paste("V", 1:numbercols, sep = ""))
  sfSel <- sf[sf$V4 %in% specInd & (sf$V4 != 0),]

  sfSelNew <- sfSel
  for(i in 1:length(specSel)){
    if(sum(sfSel$V4 == specInd[i]) != 0){
      sfSelNew[sfSel$V4 == specInd[i],]$V4 <- i
    }
  }
  #write.table(sfSelNew, file.path(TMDirectory,"InitialValues.txt"), col.names = FALSE, row.names = FALSE, sep = " ", na = " ", quote = FALSE)
  fwrite(sfSelNew, file.path(TMDirectory,"InitialValues.txt"), col.names = FALSE, row.names = FALSE, sep = " ", na = " ", quote = FALSE)
  # specIndInd <- rep(NA, length(specAll))
  # specIndInd[specInd] <- order(specInd)
  #
  # if(length(specInd) == length(specAll)){
  #   fc <- file.copy(from = stateFile, to = "InitialValues.txt", overwrite = TRUE)
  # }else{
  #
  #   sfsel <- sf[(sf$V4 != 0) & (sf$V4 %in% specInd),]
  #   sfsel$V4 <- specIndInd[sfsel$V4]
  #   sfsel[is.na(sfsel)] <- 0
  #   write.table(sfsel, "InitialValues.txt", col.names = FALSE, row.names = FALSE, sep = " ", na = " ")
  # }
  # file.copy(from = "InitialValues.txt", to = "Statefilenew.txt", overwrite = TRUE) # DB needed?
}
