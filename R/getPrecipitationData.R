#' Extract and Process Precipitation Data for a Study Area
#'
#' This function extracts precipitation data for a specified study area and optionally processes it.
#' The function supports two primary input types for extraction: raster and data.table. The `prec` parameter can be either
#' the actual data object or a file path pointing to the data.
#' If the provided precipitation data is in millimeters, it will be converted to centimeters.
#'
#' @param prec data source for precipitation. Either a path or actual the data object containing to precipitation data
#' @param studyArea studyArea. The study area for which the precipitation data will be extracted.
#' @param years numeric vector. The range of years for which the data will be extracted.
#' @param type character. Type of the precipitation data source, either "raster" or "text"
#' @param unit charcter. Unit of the precipitation data ("cm" or "mm"). If not provided for raster data, it tries to infer it from the data.
#' @param crs Coordinate reference system for the data.
#' @param interpolate logical. If TRUE, bilinear interpolation will be used during extraction; if FALSE, nearest neighbor method will be used.
#' @param aggregate logical. If TRUE, the function will aggregate raster cells if the raster's resolution is finer than the study area's.
#' @param aggregateFun character. function used to aggregate values. Either an actual function, or for the following, their name: "mean", "max", "min", "median", "sum", "modal", "any", "all", "prod", "which.min", "which.max", "sd" (sample standard deviation) and "std" (population standard deviation)
#'
#' @return A data.table with columns "lon", "lat", "year", "prec_1",...,"prec_12" containing the extracted yearly precipitation values for the specified study area.
#' 
#' @importFrom terra units rast
#' @export
getPrecData <- function(prec, studyArea, years=NA, type="raster", unit=NA, crs=NA, interpolate=TRUE, aggregate=TRUE, aggregateFun="mean"){
  # R CMD CHECK (fix)
  .SD <- NULL

  if(type=="raster" && is.na(unit)){
    unit <- unique(units(rast(prec)));
  }
  if(length(unit) > 1 || !(unit %in% c("cm","mm"))){
    stop("No valid unit for precipitation data found. Please provide the 'unit' parameter ('cm', or 'mm')")
  }
  prec <- extractData(prec, studyArea, years=years, type=type, crs=crs, interpolate=interpolate, aggregate=aggregate, aggregateFun=aggregateFun)
  if(unit == "mm"){
    print("Precipitation data in 'mm', converting to 'cm'")
    prec[,(4:ncol(prec)):=.SD[,4:ncol(prec)]/10]
  }
  colnames(prec)[4:ncol(prec)]<-paste0("prec_",(4:ncol(prec)-3))
  return(prec)
}

#' Generate Precipitation Data for a Study Area
#'
#' Generates a dataset containing precipitation values for a given study area.
#' Each cell in the study area is assigned the same precipitation value.
#' For each subsequent year, the precipitation value is incremented by `yearlyIncrement`.
#'
#' @param precValue numeric. The initial precipitation value for the study area's cells.
#' @param studyArea StudyArea. The study area for which the precipitation data will be generated.
#' @param years numeric vector. The range of years for which the data will be generated.
#' @param yearlyIncrement numeric. The yearly increment to apply to the `precValue`.
#'
#' @return A data.table with columns "lon", "lat", "year", "prec_1",...,"prec_12" containing the generated yearly precipitation values for the specified study area.
#' @export
generatePrecData <- function(precValue, studyArea, years, yearlyIncrement=0){
  return(generateYearlyStudyData(studyArea=studyArea, names=paste0("prec_",1:12), values=rep(precValue,12), years, rep(yearlyIncrement,12)))
}
