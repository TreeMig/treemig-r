#' Changes control variables according to study area. This will only change the TreeMigConfig variable
#' @templateVar config TRUE
#' @templateVar studyArea TRUE
#' @param unitOfInputData integer. One unit of input data (stock, bioClim) corresponds to x m
#' @template var_params
#' @importFrom terra res xmin xmax ymin ymax
#' @export
setCtrOptions <- function(config, studyArea, unitOfInputData = 1000){
  validate(config, class="TreeMigConfig", length=1)
  validate(studyArea, class="StudyArea", length=1)
  validate(unitOfInputData, numeric=TRUE, length=1)

  config$setOption("unitOfInputData", unitOfInputData)

  startX <- xmin(studyArea$raster)
  endX <- xmax(studyArea$raster)
  startY <- ymax(studyArea$raster)
  endY <- ymin(studyArea$raster)
  lenX <- endX - startX
  lenY <- endY - startY
  resX <- res(studyArea$raster)[1] # must be the same?
  resY <- res(studyArea$raster)[2]

  numX <- as.integer(lenX/resX) + 1
  numY <- as.integer(lenY/resY) + 1

  config$setOption("numRows", numY)
  config$setOption("numCols", numX)
  config$setOption("gridCellLength", resX * config$getOption("unitOfInputData"))

  config$setOption("lonRealStart", startX)
  config$setOption("lonRealEnd", endX)
  config$setOption("latRealStart", startY)
  config$setOption("latRealEnd", endY)

  config$setOption("startlatStart", 1)
  config$setOption("startlonStart", 1)
  config$setOption("startlatEnd", numY)
  config$setOption("startlonEnd", numX)
  return(invisible())
}

