validateInput <- function(val, option){
  err_msg <- paste("cannot set", option$ID)
  len = 1
  if(option$class == "integerarray"){
    len = length(val)
  }
  validate(val, length=len, fun=list(
    c(function(x){
        if(length(option$enum) > 0){
          return(!val %in% option$enum)
        }
        return(FALSE)
      },
      paste0("\"",val,"\" is not a valid input for ",  option$ID, ". Valid inputs: ", paste(option$enum, collapse = ", "))
    )
  ),msg=err_msg)
  switch(
    option$class,
    numeric={
      validate(val,numeric=TRUE,msg=err_msg)
      val <- format(val, nsmall = 3, digits = 3, scientific = FALSE)
    },
    character={
      validate(val,class = c("character"),msg=err_msg, fun=list(
        c(function(x){
          return(gregexpr(pattern=" ",x) != -1)
        },paste0("\"",val,"\" is not a valid input for ", option$ID,". String must not contain any spaces!")),
        c(function(x){
          return(nchar(x) == 0)
        },paste0("Input cannot be empty!"))
      )
      )
    },
    logical={
      validate(val,class = c("logical"),msg=err_msg)
      if(val){
        val <- "T"
      }else{
        val <- "F"
      }
    },
    integer={
      validate(val, integer=TRUE, msg=err_msg)
    },
    integerarray={
      validate(val, integer=TRUE, msg=err_msg)
    },
    descriptor={
      validate(val,class = c("character"), msg=err_msg)
    },
    {
      stop(paste0("Cannot change ", option$ID, ". Invalid type: ", option$class))
    }
  )
  return(val)
}

TreeMigConfigOption <- setRefClass(
  "TreeMigConfigOption",
  fields = list(
    ID = "character",
    left = "logical",
    class = "character",
    desc = "character",
    enum = "ANY",
    value = "ANY",
    valueR = "ANY"
  ),
  methods = list(
    initialize = function(ID, left, class, desc, enum = character(0)){
      .self$ID <- ID
      .self$left <- left
      .self$class <- class
      .self$enum <- enum
      .self$value <- NA
      .self$valueR <- NA
      .self$desc <- desc
    },
    setValue = function(value){
      value_formatted <- validateInput(value, .self)
      if(length(value_formatted) > 1 || .self$class == "descriptor"){
        value_formatted <- paste0(value_formatted, collapse=" ")
      }else{
        blanks <- strrep(" ", max(15-nchar(value_formatted), 0))
        if(.self$left) {
          value_formatted <- paste0(value_formatted, blanks)
        }else{
          value_formatted <- paste0(blanks, value_formatted)
        }
      }
      .self$value <- value_formatted
      .self$valueR <- value
      return()
    },
    getValue = function(){
      return(.self$valueR)
    }
  )
)

TreeMigConfig <- setRefClass(
  "TreeMigConfig",
  fields = list(
    .options = "list",
    .path2TM = "character"
  ),
  methods = list(
    loadConfig = function(controlFileName){
      print(paste0("Loading the control file from: ", file.path(.self$.path2TM, controlFileName)))
      controlFile <- file(file.path(.self$.path2TM, controlFileName))
      controlFileLines <- readLines(controlFile)
      close(controlFile)
      #go through every option, look on which line the data is saved and read that line into the config
      for(i in 1:length(.self$.options)){
        lineNumber <- i
        value <- strsplit(trimws(controlFileLines[[lineNumber]]), split=" ")[[1]][1]
        switch(
          .self$.options[[i]]$class,
          numeric={
            value <- as.numeric(value)
          },
          character={
          },
          logical={
            if(value == "T"){
              value <- TRUE
            }else if(value == "F"){
              value <- FALSE
            }else{
              stop(paste0("Cannot change ", .self$.options[[i]]$ID, ". Boolean \"", value , "\" has to  be T or F"))
            }
          },
          integer={
            value <- as.integer(value)
          },
          integerarray={
            value <- strsplit(trimws(controlFileLines[[lineNumber]]), split="!")[[1]][1]
            value <- strsplit(trimws(value), split=" ")[[1]]
            value <- na.omit(suppressWarnings(as.numeric(value)))
          },
          descriptor={
            value <- trimws(controlFileLines[[lineNumber]])
          },
          {
            stop(paste0("Cannot change ", .self$.options[[i]]$ID, ". Invalid type: ", .self$.options[[i]]$class))
          }
        )
        if(any(is.na(value))){
          stop(paste0("Cannot change ", .self$.options[[i]]$ID, ". Invalid value: ", trimws(substr(controlFileLines[[lineNumber]], 1, 15))))
        }else{
          .self$.options[[i]]$setValue(value)
        }
      }
      return(invisible())
    },
    saveConfig = function(controlFileName){
      lines <- rep("", length(.self$.options))
      for(i in 1:length(.self$.options)){
        if(.self$.options[[i]]$class=="descriptor"){
          lines[i] <- paste0("=====  ",.self$.options[[i]]$desc,"  =====")
        }else{
          lines[i] <- paste0(.self$.options[[i]]$value, "        ! ", .self$.options[[i]]$desc)
        }
      }
      controlFile <- file(file.path(.self$.path2TM, controlFileName), "w")
      writeLines(lines, con = controlFile)

      close(controlFile)
      print(paste0("Control file saved at: ", normalizePath(file.path(.self$.path2TM, controlFileName))))
      return(invisible())
    },
    setOption = function(option, value){
      for(i in 1:length(.self$.options)){
        if(tolower(.self$.options[[i]]$ID) == tolower(option)){
          .self$.options[[i]]$setValue(value)
          return(invisible())
        }
      }
      stop(paste0("Cannot set ", option, ". No such setting found."))
    },
    getOption = function(option){
      for(i in 1:length(.self$.options)){
        if(tolower(.self$.options[[i]]$ID) == tolower(option)){
          return(.self$.options[[i]]$getValue())
        }
      }
      stop(paste0("Cannot get ", option, ". No such setting found."))
    },
    #'@importFrom crayon green bold yellow
    show = function(){
      cat("----------------------------------------------------------------------\n")
      cat("Name",
          strrep(" ", 17-nchar("Name")),
          "|",
          "value          ",
          " | ",
          "type      ",
          "|",
          "description","\n")
      cat("----------------------------------------------------------------------\n")
      for(i in 1:length(.self$.options)){
        if(.self$.options[[i]]$class == "descriptor"){
         next
        }
        cat(.self$.options[[i]]$ID,
            strrep(" ", 17-nchar(.self$.options[[i]]$ID)),
            "|",
            green$bold(.self$.options[[i]]$value),
            " | ",
            .self$.options[[i]]$class,  strrep(" ", max(9-nchar(.self$.options[[i]]$class), 0)),
            "|",
            yellow$bold(trimws(strsplit(.self$.options[[i]]$desc,"|", fixed = TRUE)[[1]][1])),"\n")
        cat("----------------------------------------------------------------------\n")
      }
    }
  )
)

#' Creates a new empty TreeMig config (FFT support)
#' @param TMDirectory character. Path to TreeMig location
#' @return TreeMigConfig.
#' @importFrom methods new
#' @export
newConfig <- function(TMDirectory){
  cfg <- TreeMigConfig$new()
  cfg$.path2TM <- TMDirectory
  cfg$.options <- list(

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Simulation"),
    TreeMigConfigOption$new("simulationID", TRUE, "character",                     "File identification                  | experimentID"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Time"),
    TreeMigConfigOption$new("numYears", FALSE, "integer",                          "number of years to be simulated      | numyrs"),
    TreeMigConfigOption$new("startYear", FALSE, "integer",                         "start year                           | simustartyear"),
    TreeMigConfigOption$new("reportIntervals", FALSE, "integerarray",              "report intervals                     | reportIntervals"),
    TreeMigConfigOption$new("inoculationTime", FALSE, "integer",                     "initial simulation time              | inoculationTime"),
    TreeMigConfigOption$new("stateOutputDist", FALSE, "integer",                   "interval of statefile output for backup                      | stateOutputDist"),
    TreeMigConfigOption$new("stateOutputYears", FALSE, "integerarray",                  "specific years for statefile output (can also be empty)      | stateOutputYears"),
    TreeMigConfigOption$new("readFromStatefile", FALSE, "logical",                     "read initial state from state file?                          | readStateVect"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Space"),
    TreeMigConfigOption$new("patchArea", FALSE, "numeric",                        "patch area, area of reference for distrib                    | pltsiz"),
    TreeMigConfigOption$new("numRows", FALSE, "integer",                          "used number of rows in grid                                  | maxlat"),
    TreeMigConfigOption$new("numCols", FALSE, "integer",                          "used number of colums in grid                                | maxlon"),
    TreeMigConfigOption$new("gridCellLength", FALSE, "numeric",                   "Side-length of grid cell  (m)                                | cellSideLength"),
    TreeMigConfigOption$new("unitOfInputData", FALSE, "numeric",                  "one unit of input data (stock, bioclim)  corresponds to x m  | unitOfSpatialData"),
    TreeMigConfigOption$new("lonRealStart", FALSE, "numeric",                     "left (west) edge in real coordinates                         | lonRealStart"),
    TreeMigConfigOption$new("lonRealEnd", FALSE, "numeric",                       "right (east) edge in real coordinates                        | lonRealEnd"),
    TreeMigConfigOption$new("latRealStart", FALSE, "numeric",                     "bottom (south) edge in real coordinates                      | latRealStart"),
    TreeMigConfigOption$new("latRealEnd", FALSE, "numeric",                       "top (north) edge in real coordinates                         | latRealEnd"),
    TreeMigConfigOption$new("startlatStart", FALSE, "integer",                    "initial cell position                                        | startlatStart"),
    TreeMigConfigOption$new("startlatEnd", FALSE, "integer",                      "initial cell position                                        | startlatEnd"),
    TreeMigConfigOption$new("startlonStart", FALSE, "integer",                    "initial cell position                                        | startlonStart"),
    TreeMigConfigOption$new("startlonEnd", FALSE, "integer",                      "initial cell position                                        | startlonEnd"),
    TreeMigConfigOption$new("boundaries", FALSE, "character",                     "c:cyclic, a:absorbing , s: south-nord, w, west-east          | boundaries", c("c", "a", "s", "w")),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Environment"),
    TreeMigConfigOption$new("includeEnvInfo", FALSE, "logical",          "include environmental info for calcs?  | ienv"),
    TreeMigConfigOption$new("readEnvFromFile", FALSE, "logical",         "read environmental info from file?     | envFromfile"),
    TreeMigConfigOption$new("bioClimFile", TRUE, "character",            "file with bioclimate data"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Default Driver Values"),
    TreeMigConfigOption$new("defaultDetDistu", FALSE, "numeric",                       "default deterministic disturbances"),
    TreeMigConfigOption$new("defaultBrowsing", FALSE, "numeric",                       "default browsing"),
    TreeMigConfigOption$new("defaultDrstr", FALSE, "numeric",                       "default drought stress for germination; -1  same as DrStr for trees;germDrought"),
    TreeMigConfigOption$new("defaultNutrients", FALSE, "numeric",                       "default nutrient availability;nutrients      | nutrients "),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Dispersal"),
    TreeMigConfigOption$new("doFFT", FALSE, "logical",         "Dispersal by FFT (T) or brute force (F).            | doFFT"),
    TreeMigConfigOption$new("dispDifferent", FALSE, "logical",         "Dispersal distance different for species?           | dispersalDifferent"),
    TreeMigConfigOption$new("kernelType", FALSE, "integer",                 "0: circular, 1: single-expon. 2: double-expon.      | kernelType", c(0,1,2)),
    TreeMigConfigOption$new("epsKernel", FALSE, "numeric",                  "cut-off threshold of kernel  (in 1/1000)            | epsKernel"),
    TreeMigConfigOption$new("alphaKernel", FALSE, "numeric",                "Alpha-valuet of kernel (general)                    | alpha"),
    TreeMigConfigOption$new("activeSeedDisp", FALSE, "logical",        "Active or passive seed dispersal                    | active"),
    TreeMigConfigOption$new("stochSeedDisp", FALSE, "logical",    "Stochastic (T) or deterministic (F)seed dispersal?  | doStochSeedDisp"),
    TreeMigConfigOption$new("numOuterSeeds", FALSE, "integer",              "No. of seeds/patch  coming from outside grid        | OuterSeeds"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Seeds"),
    TreeMigConfigOption$new("diffSeedProd", FALSE, "logical",    "Seed production different for species?          | seedsDifferent"),
    TreeMigConfigOption$new("diffMaturHeight", FALSE, "logical",  "maturation heigth different for species?        | mathgtDifferent"),
    TreeMigConfigOption$new("mastSeeding", FALSE, "logical",                "Periodic seed production  ?                     | mastSeeding"),
    TreeMigConfigOption$new("seedBank", FALSE, "logical",                   "Seedbank dynamics     ?                         | seedBank"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Density"),
    TreeMigConfigOption$new("seedlingCrownDia", FALSE, "numeric",           "Diameter of seedling crowns (cm)                      | seedlcrowndiamInCm"),
    TreeMigConfigOption$new("includeSeedAnta", FALSE, "logical",            "Seed antagonists included     ?                       | withSeedAntagonists"),
    TreeMigConfigOption$new("seedAntaEff", FALSE, "numeric",                "grazing rate of seed antagonists                      | seedAntaGraz"),
    TreeMigConfigOption$new("seedAntaGraz", FALSE, "numeric",               "efficiency of grazing of seed antagonists             | seedAntaEff"),
    TreeMigConfigOption$new("seedAntaMort", FALSE, "numeric",               "mortality of seed antagonists                         | seedAntaMort"),
    TreeMigConfigOption$new("seedAntaRain", FALSE, "numeric",               "constant influx of seed  antagonists                  | seedAntaRain"),
    TreeMigConfigOption$new("inclCarryCapa", FALSE, "logical",    "Carrying capacity for seeds  included     ?           | withSeedCarrCap"),
    TreeMigConfigOption$new("seedCarrygCapa", FALSE, "numeric",       "carrying capacity of seeds /833 m^2(of each species)  | seedCarrCap"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Immigration"),
    TreeMigConfigOption$new("immigration", FALSE, "logical",                "immigration included?          | immigration"),
    TreeMigConfigOption$new("immigrationFile", TRUE, "character",   "File with species immigration  | immifilename"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Disturbances"),
    TreeMigConfigOption$new("disturbances", FALSE, "logical",                    "disturbances included ?                     | disturbances"),
    TreeMigConfigOption$new("disturbProb", FALSE, "numeric",                "disturbance probability per cell and year   | disturbProb"),
    TreeMigConfigOption$new("disturbIntens", FALSE, "numeric",              "proportion of trees which die by disturb.   | disturbIntensity"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Clumping"),
    TreeMigConfigOption$new("facVarInit", FALSE, "numeric",                 "Clumping factor for light freq.  initial.  | facvarInit"),
    TreeMigConfigOption$new("facVarDyn", FALSE, "numeric",                  "Clumping factor for light freq.  spatial   | fcvarDyn"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Light dep."),
    TreeMigConfigOption$new("estabEqHC0", FALSE, "logical",                 "light dep. of establ. for seedl. or germination  | estabEqHC0"),
    TreeMigConfigOption$new("contLightDepG", FALSE, "logical",              "light dep. of establ. continuous                 | contLightDepGerm"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Species Parameter"),
    TreeMigConfigOption$new("speciesParsFile", TRUE, "character",           "File with species pars  | specfilename"),

TreeMigConfigOption$new("descriptor", TRUE, "descriptor", "Output"),
    TreeMigConfigOption$new("outputNetcdf", FALSE, "logical",               "write output as netcdf (T) or txt (F)     | writeOutput%netcdf"),
    TreeMigConfigOption$new("writeBiomass", FALSE, "logical",        "write total species biomasses             | writeOutput%biomass"),
    TreeMigConfigOption$new("writeNumbers", FALSE, "logical",         "write total species numbers               | writeOutput%number"),
    TreeMigConfigOption$new("writecHStruct", FALSE, "logical",   "write species height structures           | writeOutput%hstruct"),
    TreeMigConfigOption$new("writeSeeds", FALSE, "logical",             "write species seeds                       | writeOutput%seeds"),
    TreeMigConfigOption$new("writeSeedAnta", FALSE, "logical",   "write species seed antagonists            | writeOutput%antagonists"),
    TreeMigConfigOption$new("writePollen", FALSE, "logical",     "write species pollen percent              | writeOutput%pollen"),
    TreeMigConfigOption$new("writeLAI", FALSE, "logical",     "write species  LAI                        | writeOutput%LAI"),
    TreeMigConfigOption$new("writeBasalArea", FALSE, "logical",     "write species  basal  area                | writeOutput%basalArea"),
    TreeMigConfigOption$new("writeNPP", FALSE, "logical",     "write species  NPP                        | writeOutput%NPP"),
    TreeMigConfigOption$new("writeSpecIngrowth", FALSE, "logical",          "write species ingrowth above 1.37 m       | writeOutput%ingrowth"),
    TreeMigConfigOption$new("writeBiodiv", FALSE, "logical",        "write biodiversities                      | writeOutput%biodiv"),
    TreeMigConfigOption$new("writeLightDistr", FALSE, "logical",     "write light distributions                 | writeOutput%light"),
    TreeMigConfigOption$new("outputWithTabs", FALSE, "logical",             "output with tabs (T) or commas (F)        | tabsepOutput")
  )
  return(cfg)
}
