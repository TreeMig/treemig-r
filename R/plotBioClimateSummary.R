#' Creates a figure of the choosen bioclimatic parameters for the selected years.
#'
#' @param simulationID The simulationID belonging to the desired TreeMig simulation.
#' @param years A vector of the years to be selected (e.g., \code{c(1900:2000)}) or \code{"all_years"}.
#' This must be a subset of the years for which the bioclimate was calculated.
#' @param bioclim.var The bioclimatic variables which should be plotted across time. Accepted input are combinations of \code{"DD"}, \code{"WiT"}, \code{"DrStr"}, or \code{"all"}).
#' @param sub.area Either a dataframe with lon/lat Coordinates or a \code{"SpatialPolygonsDataFrame"} with polygon(s). In case there are several polygons one figure per polygon will be drawn.
#' @param averaging.intervall The time window used for the moving average.
#' @param quant.boundries The lower and upper boundry for the shaded area in quantiles. e.g. \code{c(0.025,0.975)}
#' @param filename A custom name for the file without file extension. If NA the file will be named generically based on a rule set defined within the function.
#' @param ggplot.args A list of settings to be transfered to ggplot to adjust plot size and layout.
#'
#' @return All the figures are directly written to the disk. Nothing is returned within R.
#'
#' @importFrom data.table fread
#' @import dplyr
#' @importFrom tidyr pivot_longer pivot_wider
#' @importFrom slider slide_dbl
#'
#' @export
#
# Created by Daniel Scherrer (daniel.scherrer@wsl.ch)

plotBioClimateSummary <- function (simulationID, 
                                   years = "all_years", 
                                   bioclim.var = "all", 
                                   sub.area = NULL, 
                                   averaging.intervall = 10, 
                                   quant.boundries = NULL, 
                                   filename = NA, 
                                   ggplot.args = list(device = "png",
                                                      scale = 1, 
                                                      width = NA, 
                                                      height = NA, 
                                                      units = "in", 
                                                      dpi = 300, 
                                                      limitsize = TRUE)) 

{
  # Set ggplot.args defaults more efficiently
  ggplot.args <- modifyList(list(device = "png", scale = 1, width = NA, height = NA, units = "in", dpi = 300, duration = 15, limitsize = FALSE), ggplot.args)
  
  #Checking and setting the quantiles
  if (!is.null(quant.boundries) | is.numeric(quant.boundries)) {
    quantiles <- c(min(quant.boundries), 0.5, max(quant.boundries))
  }else{
    quantiles <- c(0.5,0.5,0.5)
  }
  
  #Preparing and Checking the data
  bioclim <- CheckPrepareBioClimData(simulationID = simulationID, years = years, sub.area = sub.area, quant.boundries = quant.boundries, bioclim.var = bioclim.var)
  
  #Aggregating the data for the points/polygons/totalArea
  bioclim.aggr <- aggregateBioclim(bioclim = bioclim, sub.area = sub.area, quantiles = quantiles, averaging.intervall = averaging.intervall)
  
  #Making the plots
  
  #Setting the path
  plotPath <- paste0("Plots/", simulationID, "/BioclimateSummary")
  if (!file.exists(plotPath)) {
    dir.create(plotPath, recursive = TRUE)
  }
  print(paste0("All the plots will be stored in ", getwd(), plotPath))
  
  #Making the plot
  #For the total Area
  if(is.null(sub.area)){
    plotTotalArea(bioclim.aggr = bioclim.aggr, quant.boundries = quant.boundries, filename = filename, bioclim.var = bioclim.var, ggplot.args = ggplot.args, plotPath = plotPath, simulationID = simulationID)
  }
  #For points
  if(is.data.frame(sub.area)){
    plotPoints(bioclim.aggr = bioclim.aggr, filename = filename, bioclim.var = bioclim.var, ggplot.args = ggplot.args, plotPath = plotPath, simulationID = simulationID)
  }
  #For polygons
  if (class(sub.area) == "SpatialPolygonsDataFrame") {
    plotPolygons(bioclim.aggr = bioclim.aggr, quant.boundries = quant.boundries, filename = filename, bioclim.var = bioclim.var, ggplot.args = ggplot.args, plotPath = plotPath, simulationID = simulationID)
  }
} 

##################################################################################################################
# Subfunction checking the input for the plotting of the bioclimate

CheckPrepareBioClimData <- function(simulationID, years, sub.area, quant.boundries, bioclim.var){
  
  #Reading the data
  ctr <- readCtr(simulationID = simulationID)
  bioclim <- fread(paste0("E/", ctr$getOption("bioClimFile")))
  print(paste0("Loading the bioclim file from: ", getwd(), "/E/", ctr$getOption("bioClimFile")))
  
  #Checking and filering for years
  if (!"all_years" %in% years) {
    if (length(setdiff(years, unique(bioclim$year))) != 0) {
      stop("Not all selected years are available in the bioclim input file.")
    }
    bioclim <- subset(bioclim, year %in% years)
  }
  
  #Bioclimatic variables
  if (!("all" %in% bioclim.var)) {
    if (length(setdiff(bioclim.var, colnames(bioclim))) != 0) {
      stop("Some of the selected bioclimatic variables do not exist in the bioclim input file.")
    }
    bioclim <- bioclim[, c("lon", "lat", "year", ..bioclim.var)]
  }
  
  #Subsetting the area if needed
  
  #See if subarea provided
  if (!is.null(sub.area)) {
    
    #Check if subarea is points
    if (is.data.frame(sub.area)) {
      sub.area$Plot_ID <- paste0(sub.area$lon, "_", sub.area$lat)
      sub.area$lon <- unlist(lapply(as.numeric(sub.area$lon), 
                                    function(x) bioclim$lon[which.min(abs(as.numeric(bioclim$lon) - x))]))
      sub.area$lat <- unlist(lapply(as.numeric(sub.area$lat), 
                                    function(x) bioclim$lat[which.min(abs(as.numeric(bioclim$lat) - x))]))
      bioclim <-  sub.area %>% left_join(bioclim, by = c("lon", "lat"))
      bioclim <- bioclim[order(bioclim$year), ]
    }
    
    #Check if subarea if polygons
    if (class(sub.area) == "SpatialPolygonsDataFrame") {
      temp.spatial.points <- SpatialPoints(bioclim[bioclim$year ==  min(bioclim$year), c("lon", "lat")])
      points.extract <- do.call("rbind", 
                                lapply(1:dim(sub.area)[1],  
                                       function(x) {data.frame(lon = temp.spatial.points[sub.area[x, ], ]@coords[, 1], 
                                                               lat = temp.spatial.points[sub.area[x, ], ]@coords[, 2], Plot_ID = x)}))
      bioclim <- merge(points.extract, bioclim, by = c("lon", "lat"))
      bioclim <- bioclim[order(bioclim$year, bioclim$Plot_ID), ]
    }
    
    #If subarea is provided but neither points nor a polygon return an error
    if (!is.data.frame(sub.area) & class(sub.area) != "SpatialPolygonsDataFrame") {
      stop("The information about the subarea provided is neiter a data.frame with lat/lon nor a shapefile.")
    }
  }else {
    bioclim <- add_column(bioclim, Plot_ID = 1, .after = 2)
  }
  return(bioclim)
}

##################################################################################################################
# Subfunction aggregating the bioclim per Points/Polygons or TotalArea

aggregateBioclim <- function(bioclim, sub.area, quantiles, averaging.intervall){
  
  #Aggregation for points
  if (is.data.frame(sub.area)) {
    bioclim.aggr <- bioclim %>% 
      pivot_longer(col = c(5:dim(bioclim)[2]), names_to = "Bioclim_var", values_to = "values") %>% 
      group_by(Plot_ID, Bioclim_var, year) %>% 
      summarise(mean_value = mean(values)) %>% 
      group_by(Plot_ID, Bioclim_var) %>% 
      arrange(Plot_ID, Bioclim_var, year) %>% 
      mutate(mean_smooth = slider::slide_dbl(mean_value, mean, .before = averaging.intervall/2, .after = averaging.intervall/2)) %>% 
      ungroup()
    
  }  else {
    
    #Aggregation for polygons or whole area
    bioclim.aggr <- bioclim %>% 
      pivot_longer(col = c(5:dim(bioclim)[2]), names_to = "Bioclim_var", values_to = "values") %>% 
      group_by(Plot_ID, year, Bioclim_var) %>% 
      reframe(quant_value = quantile(values, quantiles), quant_index = as.factor(c("Up", "Med", "Low"))) %>% 
      pivot_wider(names_from = "quant_index", values_from = "quant_value", names_prefix = "Q_") %>% 
      group_by(Plot_ID, Bioclim_var) %>% 
      arrange(Plot_ID, Bioclim_var, year) %>% 
      mutate(Q_Low_smooth = slider::slide_dbl(as.vector(Q_Low), mean, .before = round(averaging.intervall/2), .after = round(averaging.intervall/2)), 
             Q_Med_smooth = slider::slide_dbl(Q_Med, mean, .before = round(averaging.intervall/2), .after = round(averaging.intervall/2)), 
             Q_Up_smooth = slider::slide_dbl(Q_Up, mean, .before = round(averaging.intervall/2), .after = round(averaging.intervall/2))) %>% 
      ungroup()
  }
  return(bioclim.aggr)
}

  
  
##################################################################################################################
# Subfunction to plot the figure for the whole area

plotTotalArea <- function(bioclim.aggr, quant.boundries, filename, bioclim.var, ggplot.args, plotPath, simulationID){
  
  #Making the plot
  my.plot <- ggplot(bioclim.aggr, aes(x = year, col = "black")) + 
    geom_line(aes(y = Q_Med), col = "black", lty = 2) + 
    geom_line(aes(y = Q_Med_smooth), col = "black", lty = 1, lwd = 2) + 
    facet_wrap(~Bioclim_var, scales = "free", nrow = length(unique(bioclim.aggr$Bioclim_var)))+
    labs(x = "\nYear", y = "Bioclimatic variables\n", colour = "Quantiles") + 
    scale_color_brewer(palette = "Set1") + 
    theme_light() + 
    theme(text = element_text(family = "serif", size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10,color = "white"))
  
  #Adding quantiles as ribbon if provided
  if (!is.null(quant.boundries)) {
    my.plot <- my.plot + geom_ribbon(aes(ymax = Q_Up_smooth, ymin = Q_Low_smooth), col = "grey50", alpha = 0.5) 
  }
  
  #Saving the plot
  ggsave(ifelse(is.na(filename), 
                paste0(plotPath, "/", simulationID, "_BioClimateSummary_AllArea_", paste(bioclim.var, collapse = "_"), "_", min(bioclim.aggr$year), "to", max(bioclim.aggr$year), ".", ggplot.args$device), 
                paste0(plotPath, "/", filename, ".", ggplot.args$device)), 
         my.plot, 
         device = ggplot.args$device, 
         scale = ggplot.args$scale, 
         width = ggplot.args$width, 
         height = ggplot.args$height, 
         units = ggplot.args$units, 
         dpi = ggplot.args$dpi, 
         limitsize = ggplot.args$limitsize)
  if (interactive()) {
    print(my.plot)
  }
}  

##################################################################################################################
# Subfunction to plot the figure for point data

plotPoints <- function(bioclim.aggr, filename, bioclim.var, ggplot.args, plotPath, simulationID){
  
  #Making the plot
  my.plot <- ggplot(bioclim.aggr, aes(x = year, col = Plot_ID)) + 
    geom_line(aes(y = mean_value), lty = 2) + 
    geom_line(aes(y = mean_smooth), lty = 1, lwd = 2) + 
    facet_wrap(~Bioclim_var, scales = "free", nrow = length(unique(bioclim.aggr$Bioclim_var))) + 
    labs(x = "\nYear", y = "Bioclimatic variables\n", colour = "Points") + 
    scale_color_brewer(palette = "Set1") + 
    theme_light() + 
    theme(text = element_text(family = "serif", size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10,  color = "white"))
  
  #Saving the plot
  ggsave(ifelse(is.na(filename), 
                paste0(plotPath, "/", simulationID, "_BioClimateSummary_Points_", paste(bioclim.var, collapse = "_"), "_", min(bioclim.aggr$year),"to", max(bioclim.aggr$year), ".", ggplot.args$device), 
                paste0(plotPath, "/", filename, ".", ggplot.args$device)), 
         my.plot, 
         device = ggplot.args$device, 
         scale = ggplot.args$scale, 
         width = ggplot.args$width, 
         height = ggplot.args$height, 
         units = ggplot.args$units, 
         dpi = ggplot.args$dpi, 
         limitsize = ggplot.args$limitsize)
  
  if (interactive()) {
    print(my.plot)
  }
}

##################################################################################################################
# Subfunction to plot the figure for multiple polygons

plotPolygons <- function(bioclim.aggr, quant.boundries, filename, bioclim.var, ggplot.args, plotPath, simulationID){
  
  #Loop going through all the polygons
  for (p in 1:length(unique(bioclim.aggr$Plot_ID))) {
    
    #Making the plot for a single polygon
    my.plot <- ggplot(subset(bioclim.aggr, Plot_ID == p), aes(x = year)) + 
      geom_line(aes(y = Q_Med), col = "black", lty = 2) + 
      geom_line(aes(y = Q_Med_smooth), col = "black", lty = 1, lwd = 2) + 
      facet_wrap(~Bioclim_var,  scales = "free", nrow = length(unique(bioclim.aggr$Bioclim_var))) + 
      labs(x = "\nYear", y = "Bioclimatic variables\n", colour = "Quantiles") + 
      scale_color_brewer(palette = "Set1") + 
      theme_light() + 
      theme(text = element_text(family = "serif", size = 12), 
            plot.title = element_text(size = 12), 
            plot.caption = element_text(size = 12), 
            strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
            strip.text.x = element_text(size = 10, color = "white"), 
            strip.text.y = element_text(size = 10, color = "white"))
    
    #Adding the quantiles as ribbon if provided
    if (!is.null(quant.boundries)) {
      my.plot <- my.plot + geom_ribbon(aes(ymax = Q_Up_smooth, ymin = Q_Low_smooth), col = "grey50", alpha = 0.5)
    }
    
    #Making the plots
    ggsave(ifelse(is.na(filename), 
                  paste0(plotPath, "/", simulationID, "_BioClimateSummary_Polygon_", p, "_", paste(bioclim.var, collapse = "_"), "_", min(bioclim.aggr$year), "to", max(bioclim.aggr$year), ".", ggplot.args$device), 
                  paste0(plotPath, "/", filename, ".", ggplot.args$device)), 
           my.plot, 
           device = ggplot.args$device, 
           scale = ggplot.args$scale, 
           width = ggplot.args$width, 
           height = ggplot.args$height, 
           units = ggplot.args$units, 
           dpi = ggplot.args$dpi, 
           limitsize = ggplot.args$limitsize)
    
    if (interactive()) {
      print(my.plot)
    }
  }
}
