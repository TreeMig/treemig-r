#' Generate yearly study data for a given study area.
#'
#' This function creates a data table containing study points for each year specified,
#' with the associated column names and values for each of those years.
#'
#' @param studyArea A study area
#' @param names character vector. Names for the variables (columns).
#' @param values numeric vector. Starting values corresponding to each name.
#' @param years numeric vector. The years for which the study data should be generated.
#' @param yearlyIncrements numeric vector. Yearly increments for each value.
#'
#' @return A data.table with columns "lon", "lat", "year", and additional columns specified by the names parameter.
#' @export
generateYearlyStudyData <- function (studyArea, names, values, years, yearlyIncrements){
    points <- studyArea$getStudyPoints()
    nPoints <- nrow(points)
    nYears <- length(years)
    TPMatrix <- data.table(
        rep(points[[1]], nYears),
        rep(points[[2]], nYears),
        rep(years, each = nPoints),
        unlist(sapply(1:length(values), function(i) {
            rep(seq(from = values[i], length.out = nYears, by = yearlyIncrements[i]), each = nPoints)
        }))
    )
    colnames(TPMatrix) <- c("lon","lat","year", names)
    return(TPMatrix)
}

#' Generate study data for a given study area.
#'
#' This function creates a data table containing study points
#' with an associated value for a specified variable.
#'
#' @param studyArea studyArea. A study area.
#' @param name character. The name of the variable (column).
#' @param value numeric. The value for each study point.
#'
#' @return A data.table with columns "lon", "lat", and an additional column specified by the name parameter.
#'
#' @importFrom data.table data.table
#'
#' @export
generateStudyData <- function(studyArea, name, value){
  # R CMD CHECK (fix)
  V0 <- NULL

  dt <- data.table(studyArea$getStudyPoints())[,V0:=value]
  colnames(dt) <- c("lon", "lat", name)
  return(dt)
}

#toDataTable <- function(r, year = NA){
#    layer_index <- which(time(r) == yearSelection)
#    r <- terra::subset(r, layer_index)
#    df <- as.data.table(r, xy=TRUE)[,year:=year]
#    names <- gsub(paste0("_",year),"",colnames(df))
#    names[1] <- "lon";
#    names[2]<-"lat"
#    colnames(dt) <-names
#    setcolorder(dt,c("lon","lat","year"))
#    setorder(dt, year, -lat, lon)
#    return(dt)
#}

#' @importFrom data.table setorder dcast setcolorder
#' @importFrom terra time<- crs<- rast
toRaster <- function(dataTable, crs){
  # R CMD CHECK (fix)
  lon <- NULL; lat<-NULL; year <- NULL

  cols <-colnames(dataTable)
  timeVect <- NA
  layers = setdiff(cols, c("lon","lat","year"));
  if("year" %in% cols){
      setorder(dataTable, year, -lat, lon)
      uniqueYears <- unique(dataTable$year)
      timeVect <- as.Date(paste0(uniqueYears,"-01-01"))
      dataTable <- dcast(dataTable, lon + lat ~ year, value.var = layers)
      colOrder <- apply(expand.grid(layers, uniqueYears), 1, function(x) paste(x[1], x[2], sep="_"))
      setcolorder(dataTable, c("lon","lat",colOrder))
  }
  r <- rast(dataTable)
  crs(r) <- crs
  # check if years are included in the...
  if(length(timeVect) > 1 || !is.na(timeVect)){
      time(r) <- rep(timeVect,length(layers))
  }
  return(r)
}

#' Extract values from a raster for a given study area.
#'
#' This function extracts values from a given raster (or raster file path) based on a provided study area.
#'
#' @param raster SpatRaster or character. The file path of a raster or a SpatRaster for data extraction.
#' @param studyArea studyArea. A study area for extraction.
#' @param xy logical. If TRUE, the resulting data table will have 'lon' and 'lat' columns.
#' @param rasterCRS The Coordinate Reference System (CRS) of the raster if not specified in the raster itself.
#' @param timeFilter character. String to filter the time dimension of the raster.
#' @param interpolate logical. If TRUE, bilinear interpolation will be used during extraction; if FALSE, nearest neighbor method will be used.
#' @param aggregate logical. If TRUE, the function will aggregate raster cells if the raster's resolution is finer than the study area's.
#' @param aggregateFun character. function used to aggregate values. Either an actual function, or for the following, their name: "mean", "max", "min", "median", "sum", "modal", "any", "all", "prod", "which.min", "which.max", "sd" (sample standard deviation) and "std" (population standard deviation)
#'
#' @return A data.table containing extracted values. If xy=TRUE, the table will also have 'lon' and 'lat' columns.
#' @export
#' @importFrom terra rast time crs project aggregate as.points extract crs<- time<- res ext xmin xmax ymin ymax crop
#' @importFrom data.table setorder setcolorder as.data.table setnames year
#'
#' @note If the raster's CRS is not provided and can't be inferred from the raster itself, the function assumes it's the same as the study area's CRS.
extractRaster = function(raster, studyArea, xy = FALSE, rasterCRS=NA, timeFilter=NA,  interpolate=TRUE, aggregate=TRUE, aggregateFun = "mean"){
    # R CMD CHECK (fix)
    year<-NULL;lat<-NULL; lon<-NULL

    if (is.character(raster)) {
        raster <- rast(raster)
    }

    # checks if the raster has a CRS value and defaults to the provided value if not
    # if raster has no crs and there is no provided value, it is assumed that the raster
    # has the same projection as the study area
    if (is.na(crs(raster)) || crs(raster) == "") {
        if (is.na(rasterCRS)) {
            warning("No projection found for raster. Assuming projection is equal to studyArea's raster")
            crs(raster) <- crs(studyArea$raster)
        } else {
            crs(raster) <- rasterCRS
        }
    }

    # subset the raster to keep only the layers that match the 'timeFilter' (if provided)
    if (!is.na(timeFilter)) {
        timeValues <- time(raster)
        if (length(timeValues) == 0) {
            stop("Raster has no time dimension")
        }
        select <-  grep(timeFilter, timeValues)
        if (length(select) == 0) {
            stop(paste0("Selected years are out of bounds: ", min(timeValues), "-", max(timeValues)))
        }
        raster <- raster[[select]]
        time(raster) <- timeValues[select]
    }

    if(aggregate){
        # calculate the aggregation factor based on the ratio of study area's grid cell size to raster's resolution
        factor <- round(studyArea$.gridCellSize/res(raster)[1])

       # proceed with aggregation only if the raster's resolution is finer than the desired studyArea's grid cell size
        if(factor > 1){
            print(paste0("The raster's resolution is finer than the study area's. It will be aggregated by a factor of ", factor, "."))

            # determine the margin needed to ensure the aggregated raster fully covers the studyArea
            margin <- max(res(raster)[1] * factor)

            # calculate the expanded extent
            expanded_extent <- ext(xmin(studyArea$raster) - margin,
                    xmax(studyArea$raster) + margin,
                    ymin(studyArea$raster) - margin,
                    ymax(studyArea$raster) + margin)

            # convert the expanded extent to a set of points (allowes for projection)
            extent_vect = as.points(expanded_extent)
            crs(extent_vect) <- crs(studyArea$raster)

            # project extent to raster's CRS
            projected_extent_vect = project(extent_vect, crs(raster))

            # crop the raster to the projected expanded extent (for performance when aggregating)
            r_cropped <- crop(raster, projected_extent_vect, snap="out")
            time <- time(raster)
            # aggregate cropped raster by factor
            raster <- aggregate(r_cropped, fun=aggregateFun, fact=factor)
            if(length(time) > 1 || !is.na(time)){
              time(raster) <- time
            }
        }
    }
    # here the data extraction happens
    suppressWarnings({
        result <- as.data.table(extract(raster, project(as.points(studyArea$raster, na.rm =FALSE),crs(raster)), ID=FALSE, xy=FALSE,method = ifelse(interpolate, "bilinear","simple")))
    })
    result[, c("lon","lat"):=studyArea$getStudyPoints()]
    setnames(result, colnames(result)[(ncol(result)-1):ncol(result)], c("lon", "lat"))
    setcolorder(result, c("lon","lat"))

    # if there are multiple years, melt the data.table
    timeValues <- time(raster)
    if (length(timeValues) > 1 || !(length(timeValues) == 1 && (is.na(timeValues) || is.null(timeValues)))) {

        years <- unique(year(as.Date(timeValues)))
        nFeatures <- length(timeValues)/length(years)
        startCol <- 3 # 1: lon, 2: lat

        # Use 'do.call' to "melt" the results into a data.table with yearly data
        # lapply partitions the result data.table into yearly data.tables and rbind
        # combines them into one data.table
        result <- do.call(function(...) rbind(..., use.names = FALSE), lapply(seq_along(years), function(i) {
            # Calculate the starting column index based on the current year and the number of features.
            startIndex <- ((i-1)*nFeatures+startCol)
            # Subset 'result' data.table by selecting the longitude, latitude, and the columns corresponding to the current year.
            # Additionally, adding a new 'year' column.
            result[, c(1:2,startIndex:(startIndex-1+nFeatures)), with=FALSE][,year:=years[i]]
        }))
        setcolorder(result, c("lon","lat","year"))
        setorder(result, year,-lat,lon)
    }else{
        setorder(result, -lat,lon)
    }
    if(!xy){
        result[, c("lon","lat"):=NULL]
    }
    return(result)
}

#' Helper Function to Extract Data from Raster or Text File
#'
#' This function extracts data from either text files or raster formats. It allows for various
#' preprocessing steps including spatial filtering, interpolation and aggregation.
#'
#' @param data Raster or text data. Can be a filepath or an object in R.
#' @param studyArea A study area for extraction.
#' @param type Character string indicating type of input data: "text" or "raster".
#' @param years Optional vector to select only the specified years.
#' @param crs The Coordinate Reference System (CRS) of the raster if not specified in the input data itself.
#' @param interpolate logical. If TRUE, bilinear interpolation will be used during extraction; if FALSE, nearest neighbor method will be used.
#' @param aggregate logical. If TRUE, the function will aggregate raster cells if the raster's resolution is finer than the study area's.
#' @param aggregateFun character. function used to aggregate values. Either an actual function, or for the following, their name: "mean", "max", "min", "median", "sum", "modal", "any", "all", "prod", "which.min", "which.max", "sd" (sample standard deviation) and "std" (population standard deviation)
#'
#' @return Processed data extracted for the studyArea.
#'
#' @importFrom data.table fread
#' @importFrom terra crs rast
#' @export
#' @note If the data's CRS is not provided and can't be inferred from the data itself, the function assumes it's the same as the study area's CRS.
extractData <- function(data, studyArea, type="raster", years=NA, crs=NA, interpolate=FALSE, aggregate=TRUE, aggregateFun="mean"){
  raster <- NA
  if(type == "text"){
    # if type is "text" and a path is provided, load the table
    if (is.character(data)) {
      data <- fread(data)
    }
    colNames <- colnames(data)

     # check if CRS is provided, if not assume CRS from studyArea
    if(is.na(crs)){
      crs <- crs(studyArea$raster)
      warning("No crs provided, assuming data has same crs as the study area!")
    }
    raster <- toRaster(data, crs(studyArea$raster))
  }else{
    # if type is "raster" and a path is provided, load the raster
    if (is.character(data)) {
      raster <- rast(data)
    }
  }
  timeFilter <- NA
  if(length(years) > 1 || !is.na(years)){
    timeFilter <- paste(years, collapse="|")
  }

  # extract data based on provided parameters
  data <- extractRaster(raster, studyArea, xy=TRUE, rasterCRS=crs, timeFilter=timeFilter, interpolate=interpolate, aggregate=aggregate, aggregateFun=aggregateFun)

  # if type is "text", set the column names to original column names
  if(type == "text"){
    colnames(data) <- colNames
  }
  return(data)
}


#' @importFrom sf read_sf st_crs st_intersects st_crs<- st_as_sf st_transform
extractShapeFile <- function(path, studyArea, shapeFileCRS = NA){
  s <- read_sf(path)

  if(is.na(st_crs(s))){
    if(is.na(st_crs(shapeFileCRS))){
      warning("No projection found for shape file. Assuming projection is equal to studyArea$raster")
      st_crs(s) <- st_crs(studyArea$raster)
    }else{
      st_crs(s) <- st_crs(shapeFileCRS)
    }
  }

  points <- st_as_sf(x = data.frame(studyArea$getStudyPoints()), coords = c("x", "y"), crs=st_crs(studyArea$raster))
  points <- st_transform(points,st_crs(s))
  index <- sapply(st_intersects(points, s), function(x) if(length(x) == 0) NA_integer_ else x[1])
  return(s[index,])
}
