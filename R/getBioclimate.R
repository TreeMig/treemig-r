#' Calculate Bioclimate Data
#'
#' This function calculates bioclimate data based on input data for climate,
#' slope aspect, and bucket size. All the data should originate from the same study area
#' and maintain the same order of cells.
#'
#' @param studyArea A StudyArea object representing the study area.
#' @param climData A data frame containing climate data.
#' @param slopeAspectData A data frame containing slope aspect.
#' @param bucketSizeData A data frame containing bucket size data.
#'
#' @return A data frame containing bioclimate data including longitude, latitude, year,
#'         Degree Days (DD), Winter Temperature (WiT), and Drought Stress (DrStr).
#' @importFrom terra crds project as.points
#' @importFrom stats complete.cases
#' @export
calculateBioclimate <- function (studyArea, climData, slopeAspectData, bucketSizeData){
  # R CMD CHECK (fix)
  year <- NULL; .N <- NULL;

  validate(studyArea, class="StudyArea", length=1)
  validate(climData, class="data.frame", length=27, allow_na = TRUE) ## better check for ncol instead of length
  validate(slopeAspectData, class="data.frame", length=5, allow_na = TRUE)
  validate(bucketSizeData, class="data.frame", length=3, allow_na = TRUE)

  # todo:
  # allow files as input

  ## Handle NA's
  CMP_clim <- complete.cases(climData)
  CMP_slap <- complete.cases(slopeAspectData)
  CMP_buct <- complete.cases(bucketSizeData)
  nRowPerYear <- climData[year == year[1], .N]
  CMP_all <-  CMP_clim[1:nRowPerYear] & CMP_slap & CMP_buct

  climData <- climData[rep(CMP_all, times = nrow(climData)/nRowPerYear),]
  slopeAspectData <- slopeAspectData[CMP_all,]
  bucketSizeData <- bucketSizeData[CMP_all,]
  studyPoints <- as.points(studyArea$raster, na.rm =FALSE);
  studyPoints <- studyPoints[CMP_all,]

  stdy_pts_wgs <-crds(project(studyPoints,"EPSG:4326")) # "EPSG:4326" -> WGS84

  years <- unique(climData[["year"]])
  yearlyData <- list()
  pars<- InitializeLFIBioClim()

  a <- pars[[1]]
  b <- pars[[2]]
  kPM <- pars[[3]]
  kIcpt <- pars[[4]]
  kCw <- pars[[5]]

  slasp <- slopeAspectData[["slasp"]]
  bucket <- bucketSizeData[["bucketsize"]]
  for(y in 1:length(years)){
    clim_year <- climData[year == years[y]]
    TPVect <- clim_year[, -c(1:3)]
    TVect <- TPVect[, 1:12]
    PVect <- TPVect[, 13:24]
    PVect[PVect < 0] <- 0 # vectorized pmax(0, TPVect[13:24])
    WiT <-  pmin(TVect[[12]], TVect[[1]], TVect[[2]]) # vectorized winterTemperature
    DDSum <- DegreeDays(TVect)
    soilMoist <- SoilMoisture(TVect, PVect, bucket, a, b, kPM, kIcpt, kCw, bucket, slasp, stdy_pts_wgs[, 2])
    DrStr <- soilMoist[[1]]
    yearlyData[[y]] <- data.table(DDSum, WiT, DrStr)
  }
  allData <- do.call("rbind", yearlyData)

  BioClimateData <- data.frame(
    lon = climData[["lon"]],
    lat = climData[["lat"]],
    year = climData[["year"]],
    DD = allData[["DDSum"]],
    WiT = allData[["WiT"]],
    DrStr = allData[["DrStr"]]
  )
  return(BioClimateData)
}
