#' Prepares all the files required for TreeMig and defines the paths accordingly in the TreeMigConfig.
#' This includes writing bioclimate and stock data to the TreeMig E directory, writing species parameter file for selected species.
#' If the option readInitialStateFromStateFile is TRUE a state file (stateFile) must be specified.
#' @param config The TreeMig config
#' @param bioClim data.frame or file name. Calculated with getBioclimate()
#' @param output_name character. The output name of the bclm
#' @importFrom data.table fwrite fread
#' @export

setBioclimate <- function(config, bioClim, output_name){
  validate(config, class="TreeMigConfig")
  validate(output_name, length=1, class="character")
  ## validate paths: check if files exist?

  if(is.character(bioClim)){
    bioClim <- fread(file.path(config$.path2TM, "E", bioClim), header = TRUE)
  }

  bioClim <- bioClim[bioClim$year %in% config$getOption("startYear"):(config$getOption("startYear")+config$getOption("numYears")),]
  fwrite(format(bioClim, trim=TRUE, scientific = FALSE), file.path(config$.path2TM, "E", output_name),sep = " ")
  config$setOption("bioClimFile", output_name)
}
