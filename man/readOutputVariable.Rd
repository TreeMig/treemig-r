% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/readOutputVariable.R
\name{readOutputVariable}
\alias{readOutputVariable}
\title{Returning the data of a \code{output.variable} created in a speciefic TreeMig simulation (\code{simulationID}) for a subset of \code{years} and \code{species}.
The data can either be sourced from a netCDF file (\code{ncdf}) or from the text files (\code{txt}).
The return format can either be a multi-dimensional array (\code{array}) or as a dataframe (\code{df}).}
\usage{
readOutputVariable(
  simulationID,
  species,
  output.variable,
  years,
  sub.area = NULL,
  preferred.source = "ncdf",
  return.format = "array"
)
}
\arguments{
\item{simulationID}{The simulationID belonging to the desired TreeMig simulation.}

\item{species}{The species that should be extracted. This must be a subset of the species simulated \code{species.names}.}

\item{output.variable}{The variable to be extracted from the output data of TreeMig simulation with the \code{simulationID}.}

\item{years}{The years that should be extracted. This must be a subset of the years that were written out in the TreeMig simulation.}

\item{sub.area}{Either a dataframe with lon/lat Coordinates or a shapefile with polygon(s). This will remove all data points not selected in case only a df is returned. No effect on returning an array.}

\item{preferred.source}{The desired source to pull the data from. This can either be \code{ncdf} or the \code{txt} files.
If one is not available the other one is used. This simply helps to be more efficient depending on what is desired.}

\item{return.format}{The format in which the data should be returned.
This can either be a dataframe (\code{df}) with columns (lon, lat, year, species, var) or a multi-dimensional array (\code{array}) based on the ncdf arrangement.
This mostly depends if the data is used for spatial statistics (e.g., extracting a shapefile or points).
In that case the array is more efficient to be put into a raster brick. While for plotting with ggplot the dataframe is more efficient.}
}
\value{
Either a dataframe or a multi-dimensional array containing the information.
}
\description{
Returning the data of a \code{output.variable} created in a speciefic TreeMig simulation (\code{simulationID}) for a subset of \code{years} and \code{species}.
The data can either be sourced from a netCDF file (\code{ncdf}) or from the text files (\code{txt}).
The return format can either be a multi-dimensional array (\code{array}) or as a dataframe (\code{df}).
}
