% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/configEditor.R
\name{newConfig}
\alias{newConfig}
\title{Creates a new empty TreeMig config (FFT support)}
\usage{
newConfig(TMDirectory)
}
\arguments{
\item{TMDirectory}{character. Path to TreeMig location}
}
\value{
TreeMigConfig.
}
\description{
Creates a new empty TreeMig config (FFT support)
}
